import { ElementRef } from 'jsM/dom.js';
const serviceMenu = $('.header--li-dropdown-con');
const renderFullHeader = function(){
    ElementRef.homeBtn.fadeOut('slow' ,(function(){
        ElementRef.headerLink.fadeIn('slow');
        serviceMenu.removeClass('own-pull-left');
    }));    
}

const renderHalfHeader = function(){
    ElementRef.headerLink.not('.half-header').fadeOut('slow' , function(){
        ElementRef.homeBtn.css({
            display : 'block'    
        });
        serviceMenu.addClass('own-pull-left');
    });
    
    
}



export {
    renderFullHeader,
    renderHalfHeader
}