import { ElementRef } from 'jsM/dom.js';
import { showArrow, HideWelcomeBubble } from 'jsM/Toggles.js';
import { router } from '../router';


export function removeLoader(callback) {
    // LOADER TEMPLATE
    const loaderTemp = require('../pug/commons/loader.pug');
    // APPEND LOADER INTO BODY
    ElementRef.body.append(loaderTemp());


    /**
     * @event load
     * @summary STARTING DOING ANIMATION WHEN WINDOW LOADED
     * 
     */
    ElementRef.window.on('load', function () {
        let loader = $('#preloader');
        let loaderLogo = $('#fixed-logo');


        loaderLogo.addClass('show');
        showArrow();

        // MAKE LOGO DISAPPEAR AFTER THREE SECONDS
        setTimeout(() => {
            loaderLogo.removeClass('show');
            setTimeout(() => {
                router.navigate('/office');
                loaderLogo.remove();
                loader.addClass('hide');
                setTimeout(() => {
                    loader.remove();
                    HideWelcomeBubble();
                }, 1400);
            }, 2000);

        }, 3000);


    })

}

export function activateControllerScope(con) {
    ElementRef.body.attr('data-active-controller', con);
}
