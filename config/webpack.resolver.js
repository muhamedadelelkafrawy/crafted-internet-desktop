const path = require('path');
const resolve = {
    alias: {
        "ss": path.resolve(__dirname, '../src/stylesheets/'),
        "jsM": path.resolve(__dirname, '../src/modules/'),
        "temps" : path.resolve(__dirname , '../src/pug/') 
    },
    extensions: ['.js', '.css', 'scss', 'pug', 'png'],
    modules: [
        path.resolve(__dirname, '../node_modules'),
        path.resolve(__dirname, '../bower_component'),
    ]
};

module.exports = {
    resolve ,
}