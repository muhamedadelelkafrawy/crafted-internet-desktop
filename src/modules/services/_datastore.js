export class dataStore {
    constructor(url, query) {
        this.axios = axios;
        this.url = url;
        this.query = query;
        this.dataCon;
    }

    get() {
        const options = {
            headers: {
                Authorization: 'key PAQWEQCZXDGSDKPW912EDWCSXP213EDAY',
            }
        }
        if (process.env.NODE_ENV === "production") {
            return this.axios.get(this.url, options);
        }
        return this.axios.get(`${this.url}${this.query || ''}`, options);
    }


    post() {

    }

}