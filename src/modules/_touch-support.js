function runSectionToSection() {
    $.scrollify({
        section: ".section-block",
        scrollSpeed: 800,
        scrollbars: false,
        updateHash: false,
        overflowScroll: false,
        before: function (index, sections) {
            $(`.header-link a[data-href="${sections[index].attr('id')}"]`).click();
        }
    })
}

function destroySectionToSection () {
    $.scrollify.destroy();
}


export {
    runSectionToSection,
    destroySectionToSection 
};