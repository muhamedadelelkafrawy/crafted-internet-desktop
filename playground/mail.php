<?php

if (empty($_POST['email']) OR ! filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) OR
    empty($_POST['name']) OR empty($_POST['mobile'] OR
    empty($_POST['subject'])) OR empty($_POST['message'])
) return;

extract($_POST);

$company = ! empty($_POST['company']) ? $_POST['company'] : 'N/A';

$from = 'karim.zayed@crafted-internet.com';

$from = "Crafted Internet<$from>";

$sendTo = [
    'karim.zayed@crafted-internet.com',
    'hassanzohdy@gmail.com'
];

$message = "<div>From: <b>$name</b></div><div>Email: <b>$email</b></div><div>Mobile: <b>$mobile</b></div><div>Company: <b>$company</b></div><p>" . nl2br($message) . "</p>";

foreach ($sendTo AS $to) {
    $headers = "From: $from" . "\r\n" .
        "Reply-To: $from" . "\r\n" .
        "Content-type:text/html;charset=UTF-8" . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $message, $headers);
}