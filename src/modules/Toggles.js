import {ElementRef} from 'jsM/dom.js'

function hideArrow() {
    ElementRef.arrow.fadeOut('fast');
}

function showArrow(){
    ElementRef.arrow.fadeIn('fast');
    
}


function ToggleServicesMenu (){   
    ElementRef.servicesTrigger.on('click' , function(e){
        e.preventDefault();
    })
}

function disableBrowserScroll(){
    ElementRef.body.css('overflow-y' , 'hidden');
}

function HideWelcomeBubble(){
    setTimeout(function() {
        $('#haytham-bubble').fadeOut(1000).remove();                 
    }, 4500);
}




export {showArrow, hideArrow , ToggleServicesMenu , disableBrowserScroll , HideWelcomeBubble}
