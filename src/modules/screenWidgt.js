/**
 * Screen Widget Function Just Create Dimensions Box With Screen Sizes 
 */

 import { isProd } from './helpers/is-prod';

export function screenWidget (){
    if (!isProd) {
        $('<div />').attr('id', 'screen-widget').appendTo('body').css({
            position: 'fixed',
            bottom: 5,
            right: 5,
            border: '1px solid white',
            padding: '4px',
            color: 'white',
            zIndex: 1000,
            background : "#000"
        }).text($(window).width() + ' x ' + $(window).height());
    
    
        $(window).on('resize', function () {
            $('#screen-widget').html($(window).width() + ' x ' + $(window).height());
        });
    }
    

}

export function destroyScreenWidght(){
    $('#screen-widget').fadeOut('fast').remove();
}