 export function toggleMenu(btn) {
     let targetMenu = $(btn.data('target'));
     let headerH = btn.parents('header').outerHeight();
     targetMenu.css('top', headerH);
     targetMenu.slideDown('fast');
 }

 export function toggleSubMenu(anc){
     let targetMenu = anc.next('ul');
     targetMenu.slideDown();
 }

