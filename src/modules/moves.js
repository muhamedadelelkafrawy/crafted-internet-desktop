import {
    ElementRef
} from 'jsM/dom.js';
import {
    router
} from '../router.js';
import {
    ToggleServicesMenu,
    hideArrow,
    showArrow
} from './Toggles.js';
import {
    secTosec
} from 'jsM/services/_animation.js';



class moveTo {

    constructor(dir) {
        this.activeLink = ElementRef.header.find('.active-link');
        this.nextLink = this.activeLink.next();
        this.prevLink = this.activeLink.prev();
        this.dir = dir;
    }



    // log(){
    //     console.log(`this is active link ${this.activeLink}`);
    //     console.log(`this is next link ${this.nextLink}`);
    //     console.log(`this is prev link ${this.prevLink}`);
    // }
    static travel(target) {
        const selector = `#${target}`;
        let el;
        el = $(selector);
        // VIEW IS NOT RENDERED
        if (el.length < 1) {
            setTimeout(() => {
                el = $(selector);
                secTosec(el);
                $('html , body').animate({
                    scrollTop: el.offset().top,
                }, 500)
            }, 1000);
        } else {
            secTosec(el);
           $('html , body').animate({
                scrollTop: el.offset().top,
            }, 500)
        }
    }

    static disbale() {
        var activeController = $('body').attr('data-active-controller');
        if (activeController == "homeController") {
            return false;
        }

        return true;
    }


}


class moveUp extends moveTo {
    constructor(dir) {
        super(dir);
    }



    navigate() {
        const hrefValue = this.prevLink.children('a').attr('data-href');


        if (this.prevLink.length == 0) {
            return false;
        }
        if (this.prevLink.hasClass('header--li-dropdown-con')) {
            this.prevLink = this.prevLink.prev().prev();
        }
        if (this.prevLink.children('a').attr('data-href') !== 'contact-us') {
            showArrow();
        }

        if (hrefValue == 'portfolio') {
            this.prevLink = this.prevLink.prev();
        }

        this.activeLink.removeClass('active-link');
        this.prevLink.addClass('active-link');
        router.navigate(this.prevLink.children('a').attr('data-href'))

    }


}


class moveDown extends moveTo {
    constructor(dir) {
        super(dir)
    }



    navigate() {
        let hrefValue = this.nextLink.children('a').attr('data-href');

        // IF THERE IS NO NEXT LINK
        if (this.nextLink.length == 0) {
            return false;
        }
        // IF NEXT LINK IS PORTFOLIO OR SERVICE MENU
        // JUMP ONE STEP 
        if (this.nextLink.attr('id') == 'return-home') {
            this.nextLink = this.nextLink.next().next();
        }



        if (hrefValue == 'portfolio') {
            this.nextLink = this.nextLink.next();
        }

        hrefValue = this.nextLink.children('a').attr('data-href');


        // IF NEXT ONE IS CONTACT US
        if (hrefValue == "contact-us") {
            hideArrow();
        }

        this.activeLink.removeClass('active-link');
        this.nextLink.addClass('active-link');
        router.navigate(this.nextLink.children('a').attr('data-href'));
    }


}

class headerMove extends moveTo {
    constructor(clickedLink) {
        super();
        this.el = clickedLink;

    }


    /** 
     * @name navigate
     * @return {boolean} 
     */
    navigate() {


        /**
         * @summary IF CLICKED HEADER-LINK IS THE ACTIVE-LINK
         * @summary IF CLICKED HEADER-LINK IS EQ TO XS
         * 
         */
        if (this.el == this.activeLink || this.el.attr('id') === "xs") {
            return false;
        }


        /**
         * @summary IF CLICKED HEADER-LINK IS CONTACT-US
         * HIDE ARROW
         */
        if (this.el.children('a').attr('data-href') == "contact-us") {
            hideArrow();
        } else {
            showArrow();
        }

        this.el.addClass('active-link').siblings('li').removeClass('active-link');
        this.activeLink = this.el;
        router.navigate(this.el.children('a').attr('data-href'));
        return true;
    }
}


export {
    moveDown,
    moveUp,
    moveTo,
    headerMove
}