const temp = require('temps/services.pug');
import {
    ElementRef
} from 'jsM/dom.js';
import {
    activateControllerScope
} from 'jsM/loader.js'
import {
    niceScroll 
} from 'jsM/niceScroll.js';
import {
    backgroundLayerAnimation,
    vectorLayerAnimation,
    textLayerAnimation,
    linesLayerAnimation ,
    fireLinesLayerAnimation,
    typedOptions
} from 'jsM/services/_animation.js';
import {
    dataStore
} from 'jsM/services/_datastore.js';
import {
    hideArrow
} from 'jsM/Toggles.js';
import {
    destroyScreenWidght
} from 'jsM/screenWidgt.js';
import {
    destroySectionToSection
} from 'jsM/_touch-support.js';
import { emmiter } from './app-controller';
import { destroyNicescroll } from '../niceScroll';
import { router } from '../../router';
import { updateTitle } from '../dom';







export class serviceController {
    constructor(person) {

        // CURRENT PERSON TO GO 
        this.person = person;

        // DEVELOPMENT-URL
        this.developmentUrl = "http://localhost:3000/";
        // PROD-ULR
        this.prodUrl = window.location.origin + '/public/crafted.json';
        // CURRENT COMPONENT-SCOPE 
        this.compScope = "page_2_services";
        
        // ASSIGN SERVICE BASED ON CURRENT DEVELOPMENT
        this.DataStore = process.env.NODE_ENV === "production" ? new dataStore(this.prodUrl , this.person): new dataStore(this.developmentUrl, this.person);

    }

    /**
     * @method
     * @name serviceStartFix
     * @summary RESET SCROLL VALUE  TO "0" WHEN ENTER ANY PERSON ROUTE
     */
    serviceStartFix(){
        if (ElementRef.window.scrollTop() >= 1 ){
            $('html, body').animate({
                scrollTop : 0 ,
            }, 500)
        }
    }

    

    setModel() {
        this.DataStore.get().then(res => {
            if ( process.env.NODE_ENV === "production") {
                this.DataStore.dataCon = res.data[this.person];
            } else {
                this.DataStore.dataCon = res.data;
            }     
            this.render();
            this.view();
        }).catch(e => {console.log(e)});
    }


    redirectToContactPage() {
        $('#get-in-action').on('click', function(){
            router.navigate('/contact-us')
        })
    }

    render() {
        updateTitle(`Crafted internet - ${this.DataStore.dataCon.pageHeader}`);
        this.DataStore.dataCon.scope = this.compScope ;
        ElementRef.root.html(temp(this.DataStore.dataCon));
    }

    view() {
        this.serviceStartFix();

        // STOP HAMMER FUNCTIONALITY
        // destroySectionToSection();

        // CALL NICE SCROLL
        destroyNicescroll();
        niceScroll(this.DataStore.dataCon.mainColor);
        
        // HIDE GODOWN ARROW
        hideArrow();

        destroyScreenWidght();
        this.redirectToContactPage();

        // FIRE ALL ANIAMTIONS EFFECTS
        backgroundLayerAnimation.init();
        vectorLayerAnimation.init();
        textLayerAnimation.init();
        let typed = new Typed("h4", typedOptions);
    }

   

    init() {
        activateControllerScope('servicesController');
        this.setModel();
    }
}
