import 'ss/app.scss';
import {
    screenWidget
} from 'jsM/screenWidgt.js';
import {
    ToggleServicesMenu ,
    disableBrowserScroll
} from 'jsM/Toggles.js';

import {router} from '../../router.js';
import {eventEmmiter} from '../libs/event-emmiter.js';
import {moveTo} from 'jsM/moves.js';
import {renderFullHeader , renderHalfHeader } from 'jsM/_header-controller.js';
import {destroyNicescroll , resizeNicescroll, niceScroll} from 'jsM/niceScroll.js';








export class appController {
    constructor(){

    }

    static init(){
        screenWidget();
        ToggleServicesMenu();
    }
}

export const emmiter = new eventEmmiter();

// CHANGE HEADER-LAYOUT BASED ON CURRENT ROUTE
emmiter.on('fixHeader', function(){


    // ACTIVE ALL SPA-LINKS
    router.updatePageLinks();
    
    // IF HOME-PAGE
    if( !moveTo.disbale() ){
        renderFullHeader();
        return true ;
    }

    // IF NOT HOME PAGE 
    
    renderHalfHeader();
    $('#return-home').on('click' ,function(){
        let root = window.location.origin;

        window.location.href = root;
    })
}, 'function');


// MAKE SOME CHANGES ON NICE SCROLL
emmiter.on('fixScroll' , function() {
    
    // IF HOME-PAGE 
    // DISABLE SCROLL 
    if(!moveTo.disbale()){
        destroyNicescroll();
        disableBrowserScroll();
        return "Home-Controller-Enabled";
    }


    setTimeout(() => {
        resizeNicescroll();        
    }, 3000);
}, 'function');
