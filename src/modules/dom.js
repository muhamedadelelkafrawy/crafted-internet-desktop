const ElementRef = {
    window : $(window) ,
    root : $("#root"),
    body : $('body'),
    servicesTrigger : $('#services-trigger') ,
    servicesMenu : $('#services-trigger').next('ul') ,
    arrow :$('.arrows-down-wrapper'),
    serviceShowcase : $('.final-showcase'),
    header : $('header'),
    headerLink : $('.header-link'),
    headerList : $('.header--ul-dropdown-menu'),
    sectionBlock : $('.section-block'),
    homeBtn : $('#return-home')
};

ElementRef.mapRef = {
    mapCon : $('#contact-map') ,
    showBtn : $('#show-location'),
    closeBtn : $('#hide-map'),
    mapImg : $('#contacy-map img'),     
};


function updateTitle(pageTitle) {
    document.head.getElementsByTagName('title')[0].innerText = pageTitle;
}


export {ElementRef, updateTitle}