import {
    ElementRef
} from 'jsM/dom.js';
const { mapRef } = ElementRef;



const toggleClass = 'slide-left';



const autoMapClosing = function () {
    if ($('#contact-map').hasClass(toggleClass)) {
        setTimeout(function () {
            $('#contact-map').removeClass(toggleClass);
        }, 8000);
    }
};



const showMap = function () {
    $('#show-location').on('click', function () {
        $('#contact-map').addClass(toggleClass);
        autoMapClosing();
    })
};

const hideMap = function () {
   $('#hide-map').on('click', function () {
        $('#contact-map').removeClass(toggleClass);
    })
};






export {
    showMap,
    hideMap
}