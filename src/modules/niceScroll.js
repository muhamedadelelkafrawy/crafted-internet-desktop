/**
 * NiceScroll Function Just File NiceScroll Plugin To Add Smooth Effect For Page 
 */
import {ElementRef} from 'jsM/dom.js';

function niceScroll ($cursorColor){
    $('body').niceScroll({
        scrollspeed: 100, 
        mousescrollstep: 150, 
        cursoropacitymin : 0.5 ,
        cursorwidth : '4px',
        cursorcolor : $cursorColor,
        cursorborder: `1px solid ${$cursorColor}`, 
        horizrailenabled: false 
    });

}


function destroyNicescroll(){
    ElementRef.body.getNiceScroll().hide().remove();
}

function resizeNicescroll(){
    ElementRef.body.getNiceScroll().resize();
}

export {niceScroll ,destroyNicescroll , resizeNicescroll}
