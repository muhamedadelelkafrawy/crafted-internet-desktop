const path = require('path');
var webpack = require('webpack');



//re Design Our File Strusture By Sepearte Vendors(libs) Into Single File 
//as commons Modules Between Entry Points
const extractChunksModules = new webpack.optimize.CommonsChunkPlugin({
  names : ["common" , "vendor"] ,
  minChunks : 2 ,
})

const pluginsRef = new webpack.ProvidePlugin({
  _ : "lodash" ,
  lodash : "lodash",
  $ : "jquery" ,
  jQuery : "jquery"  
})


module.exports = {
  context : path.resolve(__dirname , 'src'),


  watch : true ,
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000 ,
    ignored : '/node_modules/'
  },



  entry: {
   vendor : ['jquery' , 'bootstrap' , 'lodash' , 'nicescroll' , 'slick-carousel'],
   app : path.resolve(__dirname , 'src/app' ),
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js',
    // publicPath : '/dist'
  },




  module: {
    rules: [
      

      //Js Loader 
      {
        test: /.js?$/,
        include: [
          path.resolve(__dirname, 'src/controllers/')
        ],
        exclude: [
          path.resolve(__dirname, 'node_modules'),
          path.resolve(__dirname, 'bower_components')
        ],
        use: [{
          loader: 'babel-loader',
          options: {
            presets: [
              ['es2015', { modules: false }]
            ]
          }
        }]

      }, 

      

      //Sass Loader
      {
        test : /\.scss$/ ,
        include : [
          path.resolve(__dirname , 'src/sass')
        ],
        exclude : [
          path.resolve(__dirname , 'node_modules')
        ],
        use: [
          "style-loader" ,
          "css-Loader",
          "sass-loader"
        ] 
      },



      //Pug Loader
      {
        test : /\.pug$/ ,
        include : [
          path.resolve(__dirname , 'src/temps')
        ],
        exclude : [
          path.resolve(__dirname , 'node_modules'),
          path.resolve(__dirname , 'src/assets/'),
          path.resolve(__dirname , 'src/controllers/'),
          path.resolve(__dirname , 'src/stylesheets/'),
        ],
        use: ['html-loader' , 'pug-html-loader'] 
      },


      //Assets Loader
      {
        test: /\.jpg($|\?)|\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
        use : [{
          loader: 'url-loader' ,
          options: { 
            limit: 10000 ,
           }
        }]
        
       }
    
    ]
  },



  resolve: {
    extensions: ['.json', '.js', '.jsx', '.css'] ,
    modules : ['node_modules' , 'bower_component' , './src/controllers/']
  },


  devtool: 'cheap-module-source-map',
  devServer : {
    compress : true ,
    stats: {
      colors: true,
      cashed : true ,
      providedExports: false
    }
  },
    


  plugins : [
    extractChunksModules, 
    pluginsRef
  ]
  




};