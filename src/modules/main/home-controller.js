const temp = require('temps/home.pug');
import {
    ElementRef
} from 'jsM/dom.js';
import {
    submitContactForm
} from 'jsM/contactForm.js';
import {
    activateControllerScope
} from 'jsM/loader.js';
import {
    moveTo,
    moveDown,
    headerMove
} from 'jsM/moves.js';

import {
    officeLight
} from 'jsM/officeLight.js';
import {
    showMap,
    hideMap
} from 'jsM/_map-controller.js';
import {
    hideArrow,
    HideWelcomeBubble
} from 'jsM/Toggles.js';
import {
    runSectionToSection
} from '../_touch-support';
import {
    dataStore
} from '../services/_datastore';
import { router } from '../../router';
import { updateTitle } from '../dom';



export class homeController {
    constructor(scope) {
        this.scope = scope;
        // this.query = "brands";
        this.url = 'https://apps.crafted-internet.com/cms/public/api/clients';
        this.dataStore = new dataStore(this.url);
        updateTitle('Crafted internet')
    }

    // MAKE SURE THAT SECTION HEIGHT TAKE THE FULL VIEW-HEIGHT
    sizing() {
        $('.section-block').css('height', $(window).height());
        $(window).on('resize', function () {
            $('.section-block').css('height', $(window).height());
        })
    }

    moveDownWhenClickOnForwardArrow() {
        ElementRef.arrow.on('click', function () {
            new moveDown('down').navigate();
        })
    }



    fireRoutingWhenClickOnAnyHeaderLink() {
        ElementRef.headerLink.on('click', function (e) {
            e.preventDefault();

            // IF WE ARE IN HOME-PAGE
            if (!moveTo.disbale()) {

                // MOVE TO SECTION THAT MATCHING CURRENT CLICKED LINK
                new headerMove($(this)).navigate();
                return true;
            }
            return false;
        })
    }
    // fireSecToSecAnimationWithRoutingWhenUsingKeyboard() {
    //     $(document).on('keyup', function (e) {
    //         if (e.which == 38) {
    //             if (!moveTo.disbale()) {
    //                 let x = new moveUp('up');
    //                 x.navigate();
    //                 return true;
    //             };
    //             return false;

    //         } else if (e.which == 40) {
    //             if (!moveTo.disbale()) {
    //                 let y = new moveDown('down');
    //                 y.navigate();
    //                 return true;
    //             };
    //             return false;

    //         } else {
    //             return false;
    //         }
    //     });
    // }

    moves() {
        if (ElementRef.body.attr('data-move') == "active") {
            return 'moveModule is already installed';
        } else {
            // this.fireSecToSecAnimationWithRoutingWhenUsingKeyboard();
            this.fireRoutingWhenClickOnAnyHeaderLink();
            this.moveDownWhenClickOnForwardArrow();
            ElementRef.body.attr('data-move', 'active');

        }


    }


    /**
     * @summary FIRE TWO SLIDERS AT PARTNERS SECTION THAT ROTATE INTO DIFFERENT DIRECTIONS
     */
    slicker() {
        function configModifier(element, options) {
            const slickCommon = {
                infinite: true,
                autoplay: true,
                dots: false,
                arrows: false
            }

            element.slick(Object.assign({}, slickCommon, options));
        }

        configModifier($('.partners-list-upper'), {
            slidesToShow: 5,
            slidesToScroll: 2,
            autoplaySpeed: 1000,
        })

        configModifier($('.partners-list-lower'), {
            slidesToShow: 7,
            slidesToScroll: 2,
            autoplaySpeed: 1000,
            rtl: true,
        })
    }

    render() {
        const _this = this;
        return this.dataStore.get().then(response => {
            ElementRef.root.html(temp({
                scope: _this.scope,
                partners: {
                    data: response.data.records.map(partner => ({
                        logo: partner.icon || partner.image || partner.logo,
                        alt: partner.name,
                    }))
                },
            }));
        })
    }


    view() {
        this.sizing();
        officeLight();
        submitContactForm();
        this.moves();
        showMap();
        hideMap();
        runSectionToSection();
        HideWelcomeBubble();
        this.slicker();
    }

    init() {
        const _this = this;
        activateControllerScope('homeController');
        this.render().then(res => {
            _this.view();
            router.updatePageLinks();
        });


        if (window.location.pathname === '/contact-us') {
            setTimeout(() => {
                hideArrow();
            }, 3000);
        }
    }
}