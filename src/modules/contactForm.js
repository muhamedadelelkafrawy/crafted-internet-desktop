export function submitContactForm() {
    $(document).on('submit', '#contact-form', function (e) {
        e.preventDefault();

        var form = $(this),
            url = form.attr('action'),
            data = form.serialize();

        alert('Thank you for contacting us!');

        form.find('input, select, textarea').val('');


        console.log(data);
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            success : function(response){
                console.log(response);
            }
        });
    });
}