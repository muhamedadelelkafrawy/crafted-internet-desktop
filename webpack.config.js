const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const Dircleaner = require('clean-webpack-plugin');
var webpack = require('webpack');
const {
  isProd,
  defineEnvPlugin
} = require('./config/webpack.env.js');
const {
  provideLibsExtending ,
  libsExtracting ,
  bundleAnalyzer
} = require('./config/webpack.plugins.js');
const {
  index,
} = require('./config/webpack.template.js');
const {
  devServer
} = require('./config/webpack.devserver.js');
const {
  resolve
} = require('./config/webpack.resolver.js');
const {
  mediaConfig ,
  cssConfig
} = require('./config/webpack.loader.js');



/**
 * 
 * @param {Boolean} prod
 * @return {Object} config 
 */
const hmrConfig = function (prod) {
  var config;
  // if Development Mode
  if (!prod) {
    //fire HMR Feature
    config = new webpack.HotModuleReplacementPlugin();
    return config;
  }

}



module.exports = {
  // Sources Context
  context: path.resolve(__dirname, 'src'),
  recordsPath: path.resolve(__dirname, 'dist/', 'records.json'),
  //if prod mode
  cache: !isProd,
  profile: true,
  target: 'web',




  entry: {
    libs: ['jquery', 'axios' ,'jquery-scrollify' ,'nicescroll', 'slick-carousel', 'bootstrap', 'wow.js', 'typed.js', 'navigo', path.resolve(__dirname, 'src/stylesheets/vendors/_vendors.scss')],
    router: path.resolve(__dirname, 'src/router.js'),
  },

  output: {
    path: path.resolve(__dirname, 'dist'),
    chunkFilename: isProd ?  'public/js/[name].min.js': 'public/js/[name].js',
    filename: isProd ? 'public/js/[name].min.js' : 'public/js/[name].js',
    publicPath: isProd ? 'https://crafted-internet.com/' : '/'
  },




  module: {
    rules: [


      //Js Loader 
      {
        test: /.js?$/,
        include: [
          path.resolve(__dirname, 'src/modules/')
        ],
        exclude: [
          path.resolve(__dirname, 'node_modules'),
          path.resolve(__dirname, 'bower_components')
        ],
        use: [{
          loader: 'babel-loader',
          options: {
            presets: [
              ['es2015', {
                modules: false
              }]
            ]
          }
        }]

      },

      //pug loader
      {
        test: /\.pug$/,
        include: path.resolve(__dirname, 'src/pug/'),
        exclude: path.resolve(__dirname, 'node_modules/'),
        use: [{
          loader: 'pug-loader'
        }]
      },



      //Sass Loader
      {
        test: /\.scss$/,
        include: [
          path.resolve(__dirname, 'src/stylesheets')
        ],
        exclude: [
          path.resolve(__dirname, 'node_modules')
        ],
        use: cssConfig(isProd)

      },



      //media Loader
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        // exclude: path.resolve(__dirname, 'node_modules/'),
        use: mediaConfig(isProd)

      },


      //fonts Loader
      {
        test: /\.(eot|svg|otf|ttf|woff|woff2)$/,
        use: [{
          loader: 'file-loader',
          query: {
            name: '[name].[ext]',
            outputPath: 'public/fonts/'
          }
        }]
      }

    ]
  },


  resolve,
  devServer,


  plugins: [
    new ExtractTextPlugin({
      allChunks: true,
      filename: 'public/css/[name].min.css',
      // if development mode
      disable: !isProd
    }),
    //Disable Source Map On vendors
    new webpack.SourceMapDevToolPlugin({
      test: /\.min\.js$/,
      filename: 'public/js/[name].js.map',
      exclude: ['libs.bundle.js', 'commons.bundle.js']
    }),

    new webpack.optimize.UglifyJsPlugin({
      include: /\.min\.js$/,
      minimize: true,
      compress: true,
      comments: false
    }),

    libsExtracting,
    provideLibsExtending,
    defineEnvPlugin,
    bundleAnalyzer ,
    new Dircleaner(['dist']),
    hmrConfig(),
    new webpack.NamedModulesPlugin(),
    index,
  ]


};