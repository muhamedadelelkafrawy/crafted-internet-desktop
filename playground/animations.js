
const linesLayerAnimation = function(){
    const featureBlocks = $('.feature');
    for (var indexer in featureBlocks) {
        if (featureBlocks.hasOwnProperty(indexer)) {
            let currentFeatureBlock = $(featureBlocks[indexer]);
            let topOffset = currentFeatureBlock.offset().top ;
            currentFeatureBlock.attr('data-own-animation-offset' , topOffset);
            if(indexer == 2) {
                return false ;
            }
        }
    }
}


const fireLinesLayerAnimation = function(sc){
    const featureBlocksOffsets = {
        featureOneOffset : [$('#0').attr('data-own-animation-offset') , '#0' , _.range(50 , 100)],
        featureTwoOffset : [$('#1').attr('data-own-animation-offset') , '#1' , _.range(50 , 100)] ,
        featureThreeOffset : [$('#2').attr('data-own-animation-offset'), '#2' , _.range(50 , 100)] 
    }

    $(window).on('scroll' , function(e){
        let scrollTop = $(window).scrollTop();
        for (let indexer in featureBlocksOffsets) {
            if (featureBlocksOffsets.hasOwnProperty(indexer)) {
                let calc = (scrollTop + 300) - featureBlocksOffsets[indexer][0];
                if(featureBlocksOffsets[indexer][2].indexOf(calc) !== -1){
                    $(featureBlocksOffsets[indexer][1]).find('.poly-small').css({
                        "animation" : "dash-small 1s ease-in-out forwards",
                        "stroke" : sc
                    });
                    $(featureBlocksOffsets[indexer][1]).find('.poly-big').css({
                        "animation" : "dash-big 1s ease-in-out forwards",
                        "stroke" : sc
                    })
                    $(featureBlocksOffsets[indexer][1]).find('.circlo').css({
                        "animation" : "circo 1s ease-in-out forwards" ,
                        "fill" : sc
                    })
                    
                }
                
            }
        }        
    })

}