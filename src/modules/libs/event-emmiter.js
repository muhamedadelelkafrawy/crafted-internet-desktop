export class eventEmmiter {
    constructor(){
        this.events  = {};        
    }



    binder(eventName , eventHandler , handlerType ){
        if(typeof eventHandler == 'function'){
            this.events[eventName].push(eventHandler);
        }
        if(handlerType == "object" ){
            for (var indexer in eventHandler) {
                if (eventHandler.hasOwnProperty(indexer)) {
                    const handler = eventHandler[indexer];
                    this.events[eventName].push(handler);
                }
            }
        }
        if(handlerType == "array"){
            for (let index = 0; index < eventHandler.length; index++) {
                let func = eventHandler[index];
                this.events[eventName].push(func);
            }
        }
    }

    on(event_name , event_handler , handler_type , callback){
        if(this.events.hasOwnProperty(event_name)){
            return callback('sorry this event is registerd before');
        }
        this.events[event_name] = [];     
        this.binder(event_name , event_handler , handler_type);
        if(callback) {
            callback('event registerd smmothly');
        }
    }

    emit(event_name , ...argu){
        let x = 0 ;
        this.events[event_name].forEach(function(func) {
            func(argu[x]);
            x++
        }, this);
    }
    
}