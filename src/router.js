// Enable Hot Module Replacement To Enhance Development process
// Must Be Removed Into Production Mode 
if (module.hot) {
    module.hot.accept(['jsM/Toggles.js', 'jsM/loader.js', 'jsM/niceScroll.js', 'jsM/screenWidgt.js', 'jsM/officeLight.js', 'jsM/moves.js', 'jsM/services/_animation.js', 'jsM/services/_datastore.js', 'jsM/main/home-controller.js', 'jsM/main/services-controller.js'], function () {
        console.log('all the dependencies have been accepted');
    });
};


import {
    appController
} from 'jsM/main/app-controller.js';
import {
    emmiter
} from 'jsM/main/app-controller.js';
import {
    homeController
} from 'jsM/main/home-controller.js';
import {
    serviceController
} from 'jsM/main/services-controller.js';
import {
    moveTo
} from 'jsM/moves.js';
import {
    ToggleServicesMenu
} from 'jsM/Toggles.js';
import {
    ElementRef
} from 'jsM/dom.js';
import { removeLoader } from './modules/loader';



const router = new Navigo(process.env.NODE_ENV === 'production' ? window.location.origin : null, false, '#!');


appController.init();



/**
 * 
 * @param {String} target - SECTION TO TRAVEL 
 */
const homeInitializer = function (target) {
    // PAGE-SCOPE
    const homeScope = 'page_1_home';


    // CREATE NEW INSTANCE AND INIT IT  
    new homeController(homeScope).init();

    if (target != 'loader') {
        // ACTIVE LINK, ACTIVE-SECTION
        $(`[data-href=${target}]`).click();
        // TRAVEL TO ACTIVE SECTION
        moveTo.travel(target);
    }
}

const homeInitializerChecker = function () {
    return ElementRef.body.attr('data-active-controller') == 'homeController';
}


router.hooks({
    after: function (params) {
        // CHANGE HEADER LAYOUT BASED ON CURRENT ROUTE
        emmiter.emit('fixHeader');

        // FIX SCROLL IN HOME-PAGE BY DISABLE-IT 
        emmiter.emit('fixScroll');

        // 
        // emmiter.emit('routeChanged');
        // Install Pages Links
        router.updatePageLinks();
    }
});



// ROOT ROUTE
router.on(function () {
    removeLoader();
});




router.on({
    '/service/development': () => {
        new serviceController('developer').init();
    },
    '/service/strategy': () => {
        new serviceController('marketeer').init()
    },
    '/service/designing': () => {
        new serviceController('creator').init();
    },
    '/service/digital-marketing': () => {
        new serviceController('digital').init();;
    },
    'office': () => {
        // IF HOME-PAGE INIT BEFORE
        if (homeInitializerChecker()) {
            moveTo.travel('office');
            return 'HomeController Setuped Before';
        }
        homeInitializer('office')
    },
    'about': () => {
        if (homeInitializerChecker()) {
            moveTo.travel('about');
            return 'HomeController Setuped Before';
        }
        homeInitializer('about')
    },
    'partners': () => {
        if (homeInitializerChecker()) {
            moveTo.travel('partners');
            return 'HomeController Setuped Before';
        }
        homeInitializer('partners');
    },
    'portfolio': () => {
        if (homeInitializerChecker()) {
            window.open(`https://crafted-internet.com/public/portfolio.pdf`);
        }
        homeInitializer('office');
    },
    'portfolio/portfolio.pdf': () => {
    },
    'contact-us': () => {
        if (homeInitializerChecker()) {
            moveTo.travel('contact-us');
            return 'HomeController Setuped Before';
        }
        homeInitializer('contact-us')
    }
}).resolve();




router.notFound(() => {
    router.navigate('/');
})


router.resolve();
export {
    router
};