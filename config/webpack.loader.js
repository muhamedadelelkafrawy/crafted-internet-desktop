const ExtractTextPlugin = require("extract-text-webpack-plugin");


/**  
 * @author Muhammed Yousrii
 * @param {Boolean} prod
 * @return {Object} config
 */
const mediaConfig = function (env) {
    var config;
    if (env) {
      config = [{
          loader: 'file-loader',
          query: {
            name: '[name].[ext]',
            outputPath: 'public/media/',
          }
        },
  
        // {
        //   loader: 'image-webpack-loader',
        //   query: {
        //     mozjpeg: {
        //       quality: '65'
        //     },
        //     pngquant: {
        //       quality: '65-90',
        //       speed: 4
        //     },
        //     gifsicle: {
        //       optimizationLevel: 3,
        //     },
        //     svgo: {
        //       cleanupAttrs: true,
        //       removeDoctype: true,
        //       removeEmptyAttrs: true,
        //       removeComments: true,
        //       minfiyStyles: true,
        //     }
        //   }
        // }

      ];
  
      return config;
    }
  
    config = [{
      loader: 'file-loader',
      query: {
        name: '[name].[ext]',
        outputPath: 'public/media/',
      }
    }];
    return config;
  }






  

/**
 * return config depend on environment
 * @author Muhammed Yousrii
 * @param {Boolean} env
 * @return {Object} config
 */
const cssConfig = function (env) {
    var config;
    //If Production mode
    if (env) {
      //Extract all Css Into single file
      config = ExtractTextPlugin.extract({
        fallback: 'style-loader',
        // publicPath: '../../',
        use: [{
            loader: 'css-loader',
            options: {
              minimize: true,
              sourceMap: true,
            }
          },
          {
            loader: 'resolve-url-loader'
          },
          {
            loader: 'sass-loader'
          }
        ]
      });
  
      return config;
    }
    // if Development Mode
    config = ['style-loader', 'css-loader', 'resolve-url-loader', 'sass-loader'];
    return config;
  }



  module.exports = {
      mediaConfig ,
      cssConfig
  }