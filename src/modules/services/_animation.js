const animateRecipes  = {
    duration : '1.5s' ,
    func : 'ease-in-out' ,
    playState : 'forwards' ,
    get lineSmall (){
       return `dash-small ${this.duration} ${this.func} ${this.playState}`;
    },
    get lineBig (){
        return `dash-big ${this.duration} ${this.func} ${this.playState}`;
    },
    get circleNormal (){
        return `circo ${this.duration} ${this.func} ${this.playState}`;
    }
};

const svgClasses = {
    lineSmall : '.poly-small' ,
    lineBig : '.poly-big' ,
    circleNormal : '.circlo'
}

const typedOptions = {
    strings: [`hello i'm The ss`, `let's Take A tour` , 'Enter My World^5000' ],
    typeSpeed: 100 ,
    backSpeed : 20 ,
    loop : true  ,
    showCursor : false 
}


const backgroundLayerAnimation = new WOW({
    boxClass : 'background-layer' ,
    animateClass : 'zoomIn' ,
    offset : 350 ,
    mobile : false  ,
    live : false ,
    callback : function(box) {
        let paraBox = $(box);
        let parentBox = paraBox.parent('.feature');
        let color = parentBox.attr('data-svg-color');
        let svg = parentBox.find('svg');
        svg.find(svgClasses.lineSmall).css({
            "animation" : animateRecipes.lineSmall ,
            "stroke" : color 
        });
        svg.find(svgClasses.lineBig).css({
            "animation" : animateRecipes.lineBig,
            "stroke" : color
        })
        svg.find(svgClasses.circleNormal).css({
            "animation" :  animateRecipes.circleNormal,
            "fill" : color
        })
    } 
})

const vectorLayerAnimation = new WOW({
    boxClass : 'vector-layer-wow' ,
    animateClass : 'bounceInDown' ,
    offset : 350 ,
    mobile : false 
})

const textLayerAnimation = new WOW({
    boxClass : 'text-layer-wow' ,
    animateClass : 'fadeInLeft' ,
    offset : 300 ,
    mobile : false,
})



const secTosec = function(self){
    const scaleInClass = 'scale-in' ;
    const scaleOutClass = 'scale-out';
    const activeClass = "active-section";
    self.addClass(activeClass).siblings().removeClass(activeClass);
    self.removeClass(scaleInClass).addClass(scaleOutClass).siblings().removeClass(scaleOutClass).addClass(scaleInClass)
}



export {
    backgroundLayerAnimation ,
    vectorLayerAnimation ,
    textLayerAnimation ,
    typedOptions ,
    secTosec
}